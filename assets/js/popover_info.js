(function ($) {
    "use strict";

    var Fonic_Info_Popover = {
        options:{
            title:"",
            content:"",
            animation:true,
            trigger:'click',
            placement:'right',
            delay:{ show: 500, hide: 100 },
            html:true
        },
        $button : '',
        init: function () {
            this._registerEventListner();
        },
        _registerEventListner:function()
        {
            $(document).on('popver-open', $.proxy(this._bind,this));

        },
        _bind:function(event,data,obj){
            console.log(data);
        },
        _registerCloseEvent:function(){
            var self = this;
            var close= function(){
                self.$button.popover('hide');
                $('.closePopver').off('click',close);
            };
            $('.closePopver').on('click',close);
        }
    };
    /**
     * Initializes the component
     */
    $(
        function () { Fonic_Info_Popover.init(); }
    );
}(jQuery));