<?php
/**
 * Becklyn GmbH.
 * User: brewing
 * Date: 16.12.11
 * Time: 12:12
 */
class Camera extends CInputWidget
{
    public $options;
    public $type;
    public $files;
    private $_model;

    public function init(){
        parent::init();
    }

    public function run(){
//        $this->getFiles($this->model);
        $this->renderTopteaser();
    }

    public function renderTopteaser(){
        $files = $this->files;
        $assets = dirname(__FILE__).'/assets';
        $baseUrl = Yii::app()->assetManager->publish($assets);
        Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/camera.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile($baseUrl.'/js/jquery.mobile.customized.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerCssFile($baseUrl.'/css/camera.css','', CClientScript::POS_HEAD);


        echo $this->render('topteaser',array('files' => $files),true);
    }

    private function getFiles(){

    }

}
