### DrMabuse Yii Blog Module

#### Version
alpha -> 0.1-rc

version | create | Developer
------------ | ------------- | ------------
alpha | 13.06.2013  | Pascal Brewing

	install /data/shema.sql

	'modules' => array(
			'blogadmin' => array(
                'class' => 'application.modules.blogadmin.BlogAdminModule'
            ),
    )
    'components' => array(
    	'user' => array(
				'allowAutoLogin'=>true,
                'class' => 'application.modules.wr_user.components.WebUser',
                'stateKeyPrefix' => 'user',
                'loginUrl' => 'wr_user/user/login'
			),
        'urlManager' => array(
			'urlFormat' => 'path',
			'rules' => array(
                  .....
			        //frontend rules
			     'blog/category/<slug>' => 'blog/category',
                 'blog/author/<slug>' => 'blog/author',
                 'blog/<slug>' => 'blog/post',
                 'blog/newComment/<post_id:\d+>' => 'blog/newComment',
			),
    )
