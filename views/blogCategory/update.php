<?php
$this->breadcrumbs=array(
    'BlogAdmin' => array('/blogadmin/blogAdmin'),
    'Update'-$model->name,
);
?>
<div class="well well-small">
    <h3>Update <i><?php echo CHtml::encode($model->name); ?></i></h3>
</div>
<div class="row-fluid">
    <div class="span8">
        <div class="tabbable tabs-top">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab4" data-toggle="tab"><i class="icon-font"></i> Category</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab4">
                    <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="span4">
        <div class="tabbable_config tabs-top">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab1" data-toggle="tab"><i class="icon-cog"></i> Seo</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab1">
                    <?php $this->renderPartial('_form_seo',array('model' => $model->seo)) ?>
                </div>
            </div>
        </div>
    </div>
</div>

