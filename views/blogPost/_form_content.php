<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'blog-post-content-form',
    'enableAjaxValidation' => true,
    'enableClientValidation'=>true,
    'action' => Yii::app()->createUrl('blogadmin/blogPostContent/update'),
    'htmlOptions' => array('enctype' => 'multipart/form-data'),
    'clientOptions'=>array(
        'validateOnSubmit'=>true,
    ),
)); ?>
<div class="alert" style="display: none"><a class="close" data-dismiss="alert" href="#">&times;</a></div>
<?php $i = 0; foreach($model->blogPostContents as $content) : ?>
    <?php
    $this->beginwidget('bootstrap.widgets.TbBox', array(
        'title' => 'Content Block - '.$content->order,
        'id' => 'content_block_'.$content->id,
        'headerIcon' => 'icon-cog',
        'htmlOptions' => array('class'=>'span11','style' => $i === 0?'margin-left:2.564102564102564%;':''),
        'headerButtons' => array(
            array(
                'class' => 'bootstrap.widgets.TbButtonGroup',
                'buttons'=>array(
                    array(
                        'label'=>'',
                        'url'=>Yii::app()->createUrl('blogadmin/blogPostContent/delete',array('id' => $content->id)),
                        'icon' => 'minus-sign white',
                        'type' => 'danger',
                        'size' => 'mini',
                        'confirm' => 'Sicher das Sie diesen Block löschen wollen ?',
                        'htmlOptions' => array(
                            'rel' => 'tooltip',
                            'title' => 'löschen',
                            'data-placement' => 'left'
                        ),
                    ),
                    array(
                        'label'=>'',
                        'url'=>Yii::app()->createUrl('blogadmin/blogPostContent/order',array('id' => $content->id,'type' => 'up')),
                        'icon' => 'arrow-up',
                        'size' => 'mini',
                        'htmlOptions' => array(
                            'rel' => 'tooltip',
                            'title' => 'nach Oben',
                            'data-placement' => 'left'
                        ),
                    ),
                    array(
                        'label'=>'',
                        'url'=>Yii::app()->createUrl('blogadmin/blogPostContent/order',array('id' => $content->id,'type' => 'down')),
                        'icon' => 'arrow-down',
                        'size' => 'mini',
                        'htmlOptions' => array(
                            'rel' => 'tooltip',
                            'title' => 'nach Unten',
                            'data-placement' => 'left'
                        ),
                    ),
                    array(
                        'label'=>'',
                        'url'=>'#',
                        'icon' => 'eye-open',
                        'size' => 'mini',
                        'buttonType' => 'button',
                        'htmlOptions' => array(
                            'rel' => 'tooltip',
                            'title' => 'close-box',
                            'data-placement' => 'left',
                            'data-box' => "#content_block_{$content->id}",
                            'class' => 'closeBox'
                        ),

                    ),
                ),
            ),
        )));
        echo $form->errorSummary($content);
        echo $form->textFieldRow($content,"[{$i}]title",array('class'=>'span5','maxlength'=>255));
        echo $form->redactorRow(
            $content,
            "[{$i}]content",
            array(
                'class'=>'span4',
                'rows'=>30,
                'options'=>array(
                    'fileUpload' => $this->createUrl('blogPostContent/fileupload',array('id' => $model->id)),
                    'imageUpload' => $this->createUrl('blogPostContent/imageUpload',array('id' => $model->id)),
                    'autosave' =>  Yii::app()->createUrl('blogadmin/blogPostContent/updateSingleContent',array('id' => $content->id)),
                    'interval' =>  60, // seconds
                    'autosaveCallback' => 'updateContent'
                )
            )
        );
        echo $form->hiddenField($content,"[{$i}]post_id",array('class'=>'span5'));
        echo $form->hiddenField($content,"[{$i}]id",array('class'=>'span5'));
        echo $form->hiddenField($content,"[{$i}]order",array('class'=>'span5'));
    $this->endWidget();
    ?>
<?php $i++; endforeach; ?>
<div class="clearFix"></div>
<br />
<div class="form-actions span12" style="float: left">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'buttonType'=>'submit',
        'type'=>'primary',
        'label'=>$model->isNewRecord ? 'Create' : 'Save',
    )); ?>
</div>
<script>
    function updateContent(data, redactor_obj)
    {   $('.alert').fadeIn('fast');
        $('.alert').text('Daten wurden upgedatet');
        $(".alert").alert()
    }
    $(function(){
        $('.closeBox').on('click',function(event){
            event.preventDefault();
            var blockId = $(this).data('box');
            if($(this).children('i').hasClass('icon-eye-open'))
            {
                $(this).children('i').removeClass('icon-eye-open');
                $(this).children('i').addClass('icon-eye-close');
                $(blockId).fadeOut('fast');
            }else{
                $(this).children('i').removeClass('icon-eye-close');
                $(this).children('i').addClass('icon-eye-open');
                $(blockId).fadeIn('fast');
            }
        })
    })
</script>
<?php $this->endWidget(); ?>