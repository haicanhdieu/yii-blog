<?php
$this->widget('bootstrap.widgets.TbButtonGroup', array(
    'buttons'=>array(
        array(
            'label'=>'add',
            'url'=>'#',
            'icon' => 'plus-sign white',
            'type' => 'success',
            'htmlOptions' => array(
                'id' => 'addNewContent'
            )
        ),
    ),
));
?>
<br />
<br />
<?php
$this->renderPartial('_form_content',array('model' => $model));
?>
<?php
$url = Yii::app()->createAbsoluteUrl('blogadmin/blogPost/addContentToPost',array('post_id' => $model->id));
Yii::app()->clientScript->registerScript(
    'addNewContent',
    "
        var addNewContentUrl = '{$url}';
        $('#addNewContent').on('click',function(event){
            event.preventDefault();
            $('#blog-post-content-form').submit();
            window.location.href = addNewContentUrl;
        });
    ",
    CClientScript::POS_READY
)
?>