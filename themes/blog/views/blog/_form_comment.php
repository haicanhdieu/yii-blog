
    <?php
    $form=$this->beginWidget('CActiveForm',array(
        'id'=>'blog-comment-form',
        'method' => 'post',
        'action' => $actionUrl,
        'htmlOptions' => array(
            'class' => 'form-horizontal'
        )
    )); ?>

    <div class="control-group">
        <?php echo $form->labelEx($model, 'author',array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'author',array('class'=>'span5','maxlength'=>128)); ?>
            <?php echo $form->error($model, 'author',array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'email',array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'email',array('class'=>'span5','maxlength'=>128)); ?>
            <?php echo $form->error($model, 'email',array('class' => 'help-inline error')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'url',array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textField($model,'url',array('class'=>'span5','maxlength'=>128)); ?>
            <?php echo $form->error($model, 'url',array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <?php echo $form->labelEx($model, 'content',array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form->textArea($model, 'content',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>
            <?php echo $form->error($model, 'content',array('class' => 'help-inline')); ?>
        </div>
    </div>
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn">send</button>
        </div>
    </div>
    <?php $this->endWidget(); ?>