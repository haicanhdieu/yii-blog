<?php setlocale(LC_TIME, "de_DE"); ?>
<?php if(isset($post->blogPostContents[0]->content)) : ?>
    <?php $text = TextHelper::truncate($post->blogPostContents[0]->content,$post->readmore_length,CHtml::link(' '.$post->title.' weiterlesen',$post->url),false,true); ?>
    <?php $sharetext = urlencode(strip_tags(TextHelper::truncate($post->blogPostContents[0]->content,125,' '.$post->url,false,true))); ?>
    <?php $shareUrl = urlencode($post->url)?>
<?php else : ?>
    <?php $text = '' ?>
    <?php $sharetext = '';$shareUrl =''; ?>
<?php endif; ?>
<div class="white-card recent-post clearfix">
    <h5 class="recent-post-header">
        <?php echo CHtml::link($post->title,$post->url) ?>
    </h5>
    <div class="post-info clearfix">
        <div class="pull-left">
            <span class="post-date"><?php echo date('d m Y',strtotime($post->create_time)).strftime("%h",strtotime($post->create_time)); ?></span>
            <a href="/theme_venera/single_post" class="post-comments">14 Comments</a>
        </div>
        <div class="pull-right">
            <div class="button-group">
                <a target="_blank" href="https://twitter.com/share?url=<?php echo $shareUrl ?>&amp;text=<?php echo $sharetext ?>" class="post-like twitter-hashtag-button" data-lang="en">
                    <span style="color:#2daddc;" class="icon-twitter-3">
                </a>
                <a class="post-like" href="https://plus.google.com/share?url=<?php echo $shareUrl ?>" onclick="javascript:window.open(this.href,'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;">
                    <span style="color: #1111DD" class="icon-google"></span>
                </a>
            </div>

        </div>
    </div>
    <?php if(isset($post->gallery->galleryPhotos['0']->id)) :?>
        <?php echo CHtml::link(CHtml::image(Yii::app()->baseUrl.'/uploads/blog_gallery/'.$post->gallery->galleryPhotos['0']->id.'950_450.jpg',''),$post->url,array('class' =>'thumbnail post-image')) ?>
    <?php endif; ?>
    <?php if(isset($post->blogPostContents[0]->content)) : ?>
        <div class="post-content separated">
            <?php echo $text ?>
        </div>
    <?php endif; ?>
</div>