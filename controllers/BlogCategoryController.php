<?php

class BlogCategoryController extends EController
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column1';
    public $homeUrl;

    public function init(){
        $this->homeUrl = Yii::app()->createUrl('blogadmin/blogAdmin');
    }
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','order','create','update','admin','delete','seo'),
                'users'=>array('*'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadCatModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BlogCategory;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BlogCategory']))
		{
			$model->attributes=$_POST['BlogCategory'];

			if($model->save()){
                if(Yii::app()->request->isAjaxRequest){
                    echo CJSON::encode(
                        array(
                            'homeUrl' => $this->homeUrl,
                            'message' =>array(
                                'type' => 'success',
                                'message' => 'Neue Kategorie wurde angelegt!'
                            )
                        )
                    );
                    Yii::app()->end();
                }
                $this->redirect(array('update','id' => $model->id));
            }
		}
        if(Yii::app()->request->isAjaxRequest)
        {
            $this->renderPartial('create',array(
                'model'=>$model,
            ));
            Yii::app()->end();
        }
		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadCatModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BlogCategory']))
		{
			$model->attributes=$_POST['BlogCategory'];
			if($model->save()){
                Yii::app()->user->setFlash('success','Kategorie wurde geupdatet!');
                $this->redirect($this->homeUrl);
            }

		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param $id
     * @throws CHttpException
     */
    public function actionDelete($id)
	{
		if(Yii::app()->request->isPostRequest)
		{
			// we only allow deletion via POST request
			$this->loadCatModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
                $this->redirect($this->homeUrl);
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('BlogCategory');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BlogCategory('search');
		$model->unsetAttributes();  // clear any default values

		if(isset($_GET['BlogCategory']))
			$model->attributes=$_GET['BlogCategory'];

        if(Yii::app()->request->isAjaxRequest){
            $this->renderPartial('admin',array(
                'model'=>$model,
            ));
            Yii::app()->end();
        }

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
     * @param $id
     * @return array|CActiveRecord|mixed|null
     * @throws CHttpException
     */
    public function loadCatModel($id)
	{
		$model=BlogCategory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='blog-category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
    /**
     * sort the column up and down
     * @param $id
     * @param $type
     */
    public function actionOrder($id, $type)
    {
        $model = BlogCategory::model()->findByPk($id);
        $criteria = new CDbCriteria();

        if ($type == 'up') {
            $sort = $model->order;
            $criteria->condition = '`order` < ' . $sort;
            $criteria->order = '`order` DESC';

            if ($beforeModel = BlogCategory::model()->find($criteria)) {
                $newSort = $beforeModel->order;

                $beforeModel->order = $sort;
                $model->order = $newSort;
                if ($beforeModel->update() && $model->update()) {
                    Yii::app()->user->setFlash('success', 'Kategorie wurde verschoben');
                    $this->redirect($this->homeUrl);
                    Yii::app()->end();
                }
            } else {
                Yii::app()->user->setFlash('error', 'Kategorie kann nicht weiter nach oben verschoben werden Sie ist bereits an erste Stelle');
                $this->redirect($this->homeUrl);
                Yii::app()->end();
            }
        }


        if ($type == 'down') {
            $sort = $model->order;
            $criteria->condition = '`order` > ' . $sort;
            $criteria->order = '`order` ASC';

            if ($beforeModel = BlogCategory::model()->find($criteria)) {
                $newSort = $beforeModel->order;

                $beforeModel->order = $sort;
                $model->order = $newSort;
                if ($beforeModel->update() && $model->update()) {
                    Yii::app()->user->setFlash('success', 'Kategorie wurde verschoben');
                    $this->redirect($this->homeUrl);
                    Yii::app()->end();
                }
            } else {
                Yii::app()->user->setFlash('success', 'Kategorie kann nicht weiter nach unten verschoben werden Sie ist bereits an letzter Stelle');
                $this->redirect($this->homeUrl);
                Yii::app()->end();
            }
        }
    }

    /**
     * @param $id
     */
    public function actionSeo($id)
    {
        $model = BlogSeo::model()->findByPk($id);

        if(isset($_POST['BlogSeo']))
        {
            $model->attributes = $_POST['BlogSeo'];

            if($model->validate())
            {
               $model->update();
                Yii::app()->user->setFlash('success', 'Seo daten wurden aktualisiert');
               $this->redirect(Yii::app()->request->urlReferrer);
            }
        }
    }
}
